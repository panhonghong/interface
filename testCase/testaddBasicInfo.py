import requests
import unittest
from testCase.testlogin import *

class TestAddBasicInfo(unittest.TestCase):
    def test_add(self):
        self.c = TestLogin()
        self.cookiescode = self.c.test_Login()
        url='http://gateway.dev-goodcape.com/api/user/setBasicInfo'
        data={
            "fullName":"testname",
            "birthday":"1999-10-20",
            "gender":"1",
            "martialStatus":"1",
            "education":"1",
            "email":"36748488@qq.com"
        }

        respon = requests.post(url,data,cookies = self.cookiescode)
        print(respon.text)
        responjson=json.loads(respon.text)
        code=responjson['code']
        self.assertEqual(code,2,msg="code非1 ，登陆失败")

if __name__ == '__main__':
    unittest.main()