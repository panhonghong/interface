#集合
a = set() #创建空集合
list1 =[1,2,3,5,2,4,4,8]
b=set(list1) #将列表变更为集合
print(b)
c = {1,3,4,5,7}#直接创建集合
print(c)
#添加
c.add(2)
print(c)
#删除
c.remove(3)
print(c)
s1=set([1,2,4,5])
s2=set([1,3,4,5])
print(s1&s2)
print(s1|s2)

###字典
d={'key1':1,'key2':2,'keys':"h"}
print("d['key1':]",d['key1']) #查看字典key对应的value
d['key1']=8
print(d['key1'])
#删除指定的键
del d['keys']
print(d)
#清空字典
d.clear()
print(d)


