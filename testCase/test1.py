print('%.2f'% 3.1415926)  #输出两位小数
print('%d'% 3.00) #整型
print('%2d' %3) #输出2位数字，右对齐
print('%2d-%02d' %(3.111,2))
print('%s-%2d' %(3.111,2))
print('%.2f-%2d' %(3.111,2))

print('name:%s,age:%d' %("lily",12))
#format格式化的两种方式
print('name:{},age:{}'.format("lily",12))
print('name:{name}，age:{age}'.format(name="lily",age=12))


# -*- coding: utf-8 -*-

L = [
    ['Apple', 'Google', 'Microsoft'],
    ['Java', 'Python', 'Ruby', 'PHP'],
    ['Adam', 'Bart', 'Lisa']
]
# 打印Apple:
print(L[0][0])
# 打印Python:
print(L[1][1])
# 打印Lisa:
print(L[2][2])