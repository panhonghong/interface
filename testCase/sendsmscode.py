import xlrd


#打开excel文件
wb = xlrd.open_workbook("testsendcode.xlsx")
#按工作簿名称定位工作表
sh = wb.sheet_by_name("sendsmscode")
#输出工作簿的有效行数
print(sh.nrows)
#输出工作簿的有效列数
print(sh.ncols)
#输出第一行第一列的值
print(sh.cell(0,0).value)
#输出所有第一行的值
print(sh.row_values(0))
#输出所有第一列的值
print(sh.col_values(0))

#将第一行的行名和第二行的值组装一起
print(dict(zip(sh.row_values(0),sh.row_values(1))))

#循环读取所有行的数据
for i in range(sh.nrows):
    print(sh.row_values(i))