
import requests
import json
import unittest
from log.newconfig import *


class TestSmsCode(unittest.TestCase):
    def test_Send_Smscode(self):
        url = 'http://gateway.dev-goodcape.com/api'
        data1 ={
                "mobile": "6789012346"
                }


        headers = {"X-Client-Id":"15107248e8d3bbf1"}
        filname = 'testnumber.txt'

        res = requests.post(url= url + "/sendSmsCodeV2",data=data1,headers=headers)
        text = res.text
        print(res.text)
        print(type(res.text))

        #反序列化为python格式
        jsontext = json.loads(text)
        #查看反序列化之后的数据类型
        print(type(jsontext))
        #从字典中取出特定的值
        code = jsontext['code']
        logging.info(code)
        smscode = jsontext['data']['code']
        logging.info(smscode)
        #判断code是否为1
        self.assertEqual(code,1,msg="code非1，获取验证码失败")
        return smscode






if __name__ == '__main__':
    code = TestSmsCode()
    smscode = code.test_Send_Smscode()
    print(smscode)






