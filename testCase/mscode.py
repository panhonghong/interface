import requests
import unittest
from common.read_excel import *
import json
from log.config import *


class TestSendSmscode(unittest.TestCase):
    def setUp(self):
        pass

    def test_smscode(self):
        data_list = excel_to_list("D:/Python38/testproject/common/testsendcode.xlsx", "sendsmscode")
        case_data = get_test_data(data_list,"test_sendsms_norma")
        if not case_data:
            print("用例不存在")
            logging.DEBUG("用例不存在")
        url = case_data.get('url')
        data = case_data.get('data')
        expr_res = case_data.get('expr_res')

        res = requests.post(url=url,data= json.loads(data))
        self.assertEqual(res.text,expr_res)

if __name__ == '__main__':
    unittest.main()


