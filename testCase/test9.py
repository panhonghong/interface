L1 = ['Hello', 'World', 18, 'Apple', None]
L2 = [x for x in L1 if isinstance(x,str)]
print(L2)

#列表表达式
l=[x*x for x in range(0,10)]
#生成器
def n():
    g=(x*x for x in range(0,10))
    return g
s=n() # 先生成一个generator对象
print(next(s))
print(next(s))
print(next(s))
print(next(s))
