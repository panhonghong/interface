#字符串
a = "h e l l o"
#列表
b=["hello",1,{"a":1,"b":2}]
d=[1,2,3,4,6,8]

f=['w','b','c','d']
#类型转换list转为str
f=''.join(f)
print(f)

#str类型转换为list
g=a.split(' ')
print(g)

#元组
c=("test",1,3,6,{"h",3,1,4})

#正反索引
print(a[0]+a[-1])
print(b[-1])
print(c[-2])

#索引溢出
#print(a[5])

#切片序列名称[开始索引序列：结束索引序列:布长] 左开右闭
print(a[1:3])
print(b[:1])
print(c[:5:2])
print(a[::-1]) #反转

#按元素遍历
for item in d:
    print(item)

#按索引遍历
for index in range(len(d)):
    print(d[index])

#按枚举遍历
for i in enumerate(d):
    print(i)

#系统函数
print(len(c))
print(max(d)+min(d))
print(sorted(d))
print(list(reversed(d))) #列表反转
print(tuple(reversed(c)))#元组反转
print(''.join(reversed(a)))#字符串反转




