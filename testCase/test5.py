#位置参数x
def power(x):
    return  x * x;

power(5)

def power1(x,n):
    s=1
    while n>0:
        n=n-1
        s=s*x
    return s
print(power1(5,2))

#默认参数,必选参数在前，默认参数在后，降低调用函数的难度
#默认参数必须指向不变对象
def power2(x,n=2):
    s=1
    while n>0:
        n=n-1
        s=s*x
    return s
print(power2(5))


#可变参数,tuple或list 允许传入0个或任意个的参数
def cal(numbers):
    sum=0
    for n in numbers:
        sum = sum + n*n
    return sum
print(cal([1,2,3,4,5]))

def cal1(*number):
    sum =0
    for n in number:
        sum =sum +n*n
    return sum
number=(1,2,3,4)
print(cal1(*number))

#关键字参数 自动组装成dict
def person(name,age,**kw):
    print('name:',name,'age:',age,'other:',kw)
print(person('bob',20,city='brijing',sex='famle'))









