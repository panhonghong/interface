from collections.abc import Iterator
from collections.abc import Iterable

a=abs(-10) #函数结果赋值给变量
print(abs(-10)) #函数调用
print(a)
f=abs #变量指向函数本身
b=f(-10) #调用变量
print(b)

#判断一个对象是否是可迭代对象
print(isinstance([],Iterable))
#判断是否是迭代器
print(isinstance([],Iterator))

it = iter([1,2,3,4])
while True:
    try:
        x=next(it)
        print(x)
    except StopIteration:
        break
