'''
利用切片操作，实现一个trim()函数，去除字符串首尾的空格，注意不要调用str的strip()方法：
'''
def trim(s):
    if s[0] !=' ':
        if s[-1] != ' ':
            return s
        else:
            return s[:-2:]
    else:
        if s[-1] != ' ':
            return s[2::]
        else:
            return s[2:-2:]
if trim('hello  ') != 'hello':
    print('1测试失败!')
elif trim('  hello') != 'hello':
    print(trim('  hello'))
    print('2测试失败!')
elif trim('  hello  ') != 'hello':
    print('3测试失败!')
elif trim('  hello  world  ') != 'hello  world':
    print('4测试失败!')
#elif trim('') != '':
    #print('5测试失败!')
elif trim('    ') != '':
    print('6测试失败!')
else:
    print('测试成功!')