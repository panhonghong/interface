import time
import requests
import  json
import urllib,hashlib
from log.newconfig import *


url = "http://loach.dev-goodcape.com/openapi"
#获取当前的时间戳
stamp = time.time()
print(stamp)
#转换为int类型的13位时间戳
timestam = int(round(stamp*1000))
print(timestam)
time_new = str(timestam)
print(type(time_new))


data ={
    'appId':'3301820001',
    "outTradeNo":"20201026001",
    "amount":"100",
    "body":"test",
    "timestamp":time_new,
    "notifyUrl":"http://localhost:8080/loach/notify",
    "upi":"1234567890",
    "refNo":"029338561280",
}



#将键按照字典序排序
data_order = sorted(data.items(),key=lambda x:x[0],reverse=False)
print(data_order)
#将列表转化为字典
data_new = dict(data_order)
print(data_new)

#将key=value键值对进行url编码
datanew = urllib.parse.urlencode(data_new)
print(datanew)
print(type(datanew))


datadic = datanew + "&key=BF1BDE5A649724056F904A9335B1C1C7"
print(datadic)


#创建md5对象
m = hashlib.md5()

m= hashlib.md5(datadic.encode())
data_md5 = m.hexdigest()
#将小写字母切换为大写字母
newdata=data_md5.upper()
print(newdata)

parame ={
    'appId':'3301820001',
    "outTradeNo":"20201026001",
    "amount":"100",
    "body":"test",
    "timestamp":time_new,
    "notifyUrl":"http://localhost:8080/loach/notify",
    "upi":"1234567890",
    "sign":newdata,
    "refNo":"029338561280",
}

headers = {
    "Content-Type":"application/json"
}
respon = requests.post(url = url+"/order/create",data=json.dumps(parame),headers=headers)
print(respon.text)
