
import unittest
import HTMLTestRunner
import time

def all_case():
    #指定测试用例存放得路径
    case_dir = "D:/interfacecode/interface/testCase"
    
    testcase = unittest.TestSuite()
    discover = unittest.defaultTestLoader.discover(case_dir,
                                                   pattern="test*.py",
                                                   top_level_dir=None)
    testcase.addTests(discover)
    print(testcase)
    return testcase

if __name__=="__main__":
    runner = unittest.TextTestRunner()

    timestr = time.strftime('%Y-%m-%d-%H%M%S',time.localtime(time.time()))
    report_path = "D:/interfacecode/interface/testReport/report"+ timestr +".html"
    
    fp = open(report_path,"wb")
    runner = HTMLTestRunner.HTMLTestRunner(stream=fp,
                                           title=u"接口测试",
                                           description=u"用例执行情况")
    runner.run(all_case())
    fp.close()