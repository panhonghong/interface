import logging
import time


class GetLog:
    def __int__(self):
        return 0

    def get_log(self,level,msg):
        #创建日志收集器
        logger = logging.getLogger()
        logger.setLevel('DEBUG')

        #创建handler
        fh = logging.FileHandler(self.logname,'a',encoding='gbk')
        fh.setLevel('INFO')
        ch = logging.StreamHandler()
        ch.setLevel('INFO')

        # 定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(levelname)s [%(funcName)s: %(filename)s, %(lineno)d] %(message)s')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)


        logger.addHandler(fh)
        logger.addHandler(ch)

        if level == 'DEBUG':
            logger.debug(msg)
        elif level == 'INFO':
            logger.info(msg)
        elif level == 'WARNING':
            logger.warning(msg)
        elif level == 'ERROR':
            logger.error(msg)
        elif level == 'CRITICAL':
            logger.critical(msg)

        logger.removeHandler(fh)
        logger.removeHandler(ch)
        fh.close()

    def log_debug(self,msg):
        self.get_log('DEBUG',msg)

    def log_info(self,msg):
        self.get_log('INFO',msg)

    def log_warning(self,msg):
        self.get_log('WARNING',msg)

    def log_error(self,msg):
        self.get_log('ERROR',msg)

    def log_critical(self,msg):
        self.get_log('CRITICAL',msg)

if __name__ == '__main':
    log = GetLog()
    log.log_info("---自动化用例开始")




#logger = logging.getLogger(__name__)
#logger.setLevel(level=logging.DEBUG)
#handler = logging.FileHandler("log.txt",encoding="utf-8",mode="a")
#handler.setLevel(logging.DEBUG)
#..

